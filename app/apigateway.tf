resource "aws_lambda_permission" "this" {
 statement_id  = "AllowExecutionFromAPIGateway"
 action        = "lambda:InvokeFunction"
 function_name = aws_lambda_function.this.function_name
 principal     = "apigateway.amazonaws.com"

 source_arn = "${aws_api_gateway_rest_api.demo_api.execution_arn}/*/*"
}

# TO FILL IN resources, including but not limited to
# - aws_api_gateway_rest_api
# - aws_api_gateway_method
# - aws_api_gateway_deployment


# Create REST API gateway
resource "aws_api_gateway_rest_api" "demo_api"{
    name = "assignment_api"
}

# Create Resource 
resource "aws_api_gateway_resource" "demo_resource" {
  parent_id   = aws_api_gateway_rest_api.demo_api.root_resource_id
  path_part   = "{proxy+}"
  rest_api_id = aws_api_gateway_rest_api.demo_api.id
}

# Create Method
resource "aws_api_gateway_method" "demo_method" {
  rest_api_id   = aws_api_gateway_rest_api.demo_api.id
  resource_id   = aws_api_gateway_resource.demo_resource.id
  http_method   = "ANY"
  authorization = "NONE"
  api_key_required = true
}

# Integrate the Resource and Method
resource "aws_api_gateway_integration" "demo_integration" {
  rest_api_id = aws_api_gateway_rest_api.demo_api.id
  resource_id = aws_api_gateway_resource.demo_resource.id
  http_method = aws_api_gateway_method.demo_method.http_method
  integration_http_method = "POST"
  type = "AWS_PROXY"
  uri = aws_lambda_function.this.invoke_arn
}

# Create a Method for root
resource "aws_api_gateway_method" "demo_proxy_root" {
  rest_api_id   = aws_api_gateway_rest_api.demo_api.id
  resource_id   = aws_api_gateway_rest_api.demo_api.root_resource_id
  http_method   = "ANY"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "demo_lambda_root" {
  rest_api_id = aws_api_gateway_rest_api.demo_api.id
  resource_id = aws_api_gateway_method.demo_proxy_root.resource_id
  http_method = aws_api_gateway_method.demo_proxy_root.http_method

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.this.invoke_arn
}


# Create a deployment stage named test
resource "aws_api_gateway_deployment" "demo_deployment" {
  depends_on = [
    "aws_api_gateway_integration.demo_integration",
    "aws_api_gateway_integration.demo_lambda_root"
  ]

  rest_api_id = "${aws_api_gateway_rest_api.demo_api.id}"
  stage_name  = var.stage
}


# add an API key

resource "aws_api_gateway_api_key" "demo_api_key" {
  name = "helloworld-localstack-test"
}

resource "aws_api_gateway_usage_plan" "demo_api_usage_plan" {
  name = "usage_demo_plan"

  api_stages {
    api_id = "${aws_api_gateway_rest_api.demo_api.id}"
    stage  = "${aws_api_gateway_deployment.demo_deployment.stage_name}"
  }
}

resource "aws_api_gateway_usage_plan_key" "demo_api_usage_plan_key" {
  key_id        = aws_api_gateway_api_key.demo_api_key.id
  key_type      = "API_KEY"
  usage_plan_id = aws_api_gateway_usage_plan.demo_api_usage_plan.id
}

## custom domain

resource "aws_api_gateway_domain_name" "demo_domain" {
  domain_name              = "helloworld.myapp.earth"
  }